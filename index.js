'use strict';

function getPlayers() {
    return window.location.search.substr(1).split('&');
}

const game = new Game(getPlayers());
const view = new GameView();
const controller = new GameController(game, view);
controller.startTurn();
