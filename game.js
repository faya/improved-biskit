'use strict';

let _players;
let _currentPlayerIndex;
let _currentGameTurn;
let _dices;
let _allRules;
let _currentHatIndex;
let _scores;

const NUMBER_DICES = 2;
const HAT_RULE_VALUE = 3;

class Game {
    constructor(players) {
        this.setupNewGame(players);
    }

    get currentPlayer() { return _players[_currentPlayerIndex]; }
    get players() { return _players; }
    get dices() { return _dices; }
    get scores() { return _scores; }
    get activeRules() { return _allRules.filter(r => r.isActive(this)); }
    get isBiskit() { return _dices.reduce((agg, curr) => agg + curr) === 7; }
    get isPairOfDices() {
        let isPair = true;
        for (let i = 0; i < _dices.length - 1; i++) {
            isPair = _dices[i] === _dices[i + 1];
        }
        return isPair;
    }
    get previousPlayer() {
        let previous = _currentPlayerIndex - 1;
        if (previous < 0) previous = _players.length - 1;
        return _players[previous];
    }
    get nextPlayer() {
        let next = _currentPlayerIndex + 1;
        if (next >= _players.length) next = 0;
        return _players[next];
    }
    get randomPlayer() {
        const random = this.getRandomInt(0, _players.length - 1);
        return _players[random];
    }
    get isHatRuleActive() {
        return _dices.reduce((agg, curr) => agg || (curr === 3), false);
    }
    get isCurrentPlayerHat() {
        return _currentPlayerIndex === _currentHatIndex;
    }
    get currentHatPlayer() {
        return _players[_currentHatIndex];
    }
    get hatRuleText() {
        if (_currentHatIndex === undefined) {
            _currentHatIndex = _currentPlayerIndex;
            return `3 ! ${this.currentPlayer} takes the hat.`;
        }
        else if (this.isCurrentPlayerHat) {
            _currentHatIndex = undefined;
            return `3 ! ${this.currentPlayer} removes the hat.`;
        }
        else if (!this.isCurrentPlayerHat) {
            let nbSips = 0;
            for (const dice of _dices) {
                nbSips += dice === 3 ? 1 : 0;
            }
            this.increaseScore(this.currentHatPlayer, nbSips);
            return `3 ! ${this.currentHatPlayer} (hat) drinks 1.`;
        }
        return '';
    }

    setupNewGame(players) {
        _players = players;
        _currentPlayerIndex = 0;
        _currentGameTurn = 1;
        _allRules = [Rule.BISKIT, Rule.HAT];
        _currentHatIndex = undefined;

        _dices = [];
        this.resetDices();
        _scores = [];
        for (let i = 0; i < _players.length; i++) {
            _scores[i] = 0;
        }
    }

    initTurn() {
        this.resetDices();
    }

    playTurn() {
        for (let i = 0; i < NUMBER_DICES; i++) {
            _dices[i] = this.getRandomInt(1, 6);
        }
    }

    endTurn() {
        for (let rule of this.activeRules) {
            rule.updateScore(this);
        }
        _currentPlayerIndex++;
        if (_currentPlayerIndex === _players.length) {
            _currentPlayerIndex = 0;
            _currentGameTurn++;
        }
    }

    getRandomInt(min, max) {
        return Math.floor(Math.random() * max) + min;
    }

    resetDices() {
        for (let i = 0; i < NUMBER_DICES; i++) {
            _dices[i] = '#';
        }
    }

    submitRule(rule) {
        _allRules.push(rule);
    }

    increaseScore(targeting, value) {
        let player;
        switch (targeting) {
            case 'everyone':
                player = targeting;
                break;
            case 'current':
                player = this.currentPlayer;
                break;
            case 'previous':
                player = this.previousPlayer;
                break;
            case 'next':
                player = this.nextPlayer;
                break;
            case 'random':
                player = this.randomPlayer; // bug
                break;
            default:
                player = targeting;
                break;
        }
        
        if (player === 'everyone') {
            for (let i = 0; i < _players.length; i++) _scores[i] += value;
            return;
        }

        const playerIndex = _players.indexOf(player);
        if (playerIndex === -1) return;
        _scores[playerIndex] += value;
    }
}