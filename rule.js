'use strict';

const PREDICATES = {
    'equal': (a, b) => a === b,
    'lower': (a, b) => a <= b,
    'greater': (a, b) => a >= b,
    'even': (a, b) => (a % 2) === 0,
    'odd': (a, b) => (a % 2) === 1
};

const CONDITION_TEXTS = {
    'atleast': 'At least one dice is',
    'sum': 'The dices sum is',
    'all': 'Every dice is'
};

const PREDICATES_TEXTS = {
    'equal': 'equal to',
    'lower': 'lower than',
    'greater': 'greater than',
    'even': 'even',
    'odd': 'odd'
};

class Rule {
    constructor(isNumber, form) {
        this.copyForm(isNumber, form);
        if (isNumber) this.initNumberRule();
        else this.initFreeRule();
    }

    copyForm(isNumber, form) {
        if (isNumber) {
            this.form = {
                condition: form.condition.value,
                match: form.match.value,
                value: form.value.value,
                target: form.target.value,
                sips: form.sips.value
            };
        }
        else {
            this.form = {
                free: form.content.value
            };
        }
    }

    isActive(game) {
        return this.predicate(game);
    }

    initNumberRule() {
        const match = PREDICATES[this.form.match];
        const matchValue = parseInt(this.form.value, 10);
        this.isNumberRule = true;
        this.predicate = (game) => {
            if (this.form.condition === 'atleast') 
                return game.dices.find(d => match(d, matchValue)) !== undefined;
            if (this.form.condition === 'sum')
                return match(game.dices.reduce((agg, curr) => agg + curr), matchValue);
            if (this.form.condition === 'all')
                return game.dices.find(d => !match(d, matchValue)) === undefined;
            return false;
        };
        this.displayText = (game) => {
            let text = `${CONDITION_TEXTS[this.form.condition]} ${PREDICATES_TEXTS[this.form.match]}`;
            if (this.form.match !== 'even' && this.form.match !== 'odd')
                text += ` ${matchValue}`;
            text += `. ${this.getTarget(this.form.target, game)} drink(s) ${this.form.sips}.`;
            return text;
        };
        this.updateScore = (game) => {
            game.increaseScore(this.form.target, parseInt(this.form.sips, 10));
        };
    }

    getTarget(targeting, game) {
        switch (targeting) {
            case 'everyone': return 'Everyone';
            case 'current': return `${game.currentPlayer} (current)`;
            case 'previous': return `${game.previousPlayer} (previous)`;
            case 'next': return `${game.nextPlayer} (next)`;
            case 'random': return `${game.randomPlayer} (random)`;
            default: return '';
        }
    }

    initFreeRule() {
        this.isFreeRule = true;
        this.predicate = () => true;
        this.text = this.form.free;
        this.displayText = () => this.text;
        this.updateScore = () => {};
    }
}
Rule.BISKIT = {
    isFreeRule: true,
    predicate: (game) => game.isBiskit,
    isActive: (game) => game.isBiskit,
    displayText: () => 'BISKIT !',
    updateScore: () => {}
};
Rule.HAT = {
    isFreeRule: true,
    isActive: (game) => game.isHatRuleActive,
    displayText: (game) => game.hatRuleText,
    updateScore: () => {}
};