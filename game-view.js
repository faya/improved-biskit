'use strict';

let _screens;

class GameView {
    constructor() {
        _screens = this.initScreens();
        this.bindInteractions();
        this.checkFormState();
    }

    get isNumberRule() {
        return _screens.ruleEditor.numberRuleForm.isActive();
    }

    get numberForm() {
        return _screens.ruleEditor.numberRuleForm;
    }

    get freeForm() {
        return _screens.ruleEditor.freeRuleForm;
    }

    get activeRuleForm() {
        return this.isNumberRule ? this.numberForm : this.freeForm;
    }

    initScreens() {
        const self = this;
        const screens = {
            navbar: {
                scores: document.getElementById('scoreButton'),
                restart: document.getElementById('restartButton')
            },
            main: {
                container: document.getElementById('mainGame'),
                game: {
                    container: document.getElementById('diceContainer'),
                    playerText: document.getElementById('playerText'),
                    scoreText: document.getElementById('scoreText'),
                    playButton: document.getElementById('playButton')
                },
                rules: {
                    container: document.getElementById('rulesList'),
                    dice: document.getElementById('diceRulesList'),
                    free: document.getElementById('freeRulesList'),
                    inactive: document.getElementById('inactiveRulesList')
                },
                addRuleFirst: document.getElementById('addRuleFirst')
            },
            ruleEditor: {
                container: document.getElementById('ruleEditor'),
                scoreText: document.getElementById('ruleScoreText'),
                numberRuleForm: {
                    container: document.getElementById('numberRuleForm'),
                    activator: document.getElementById('navRuleNumber'),
                    condition: document.getElementById('conditionSelect'),
                    match: document.getElementById('matchSelect'),
                    value: document.getElementById('valueInput'),
                    target: document.getElementById('targetSelect'),
                    sips: document.getElementById('sipsInput'),
                    isActive: self.isActive.bind(self, document.getElementById('numberRuleForm'))
                },
                freeRuleForm: {
                    container: document.getElementById('freeRuleForm'),
                    activator: document.getElementById('navRuleFree'),
                    content: document.getElementById('freeRuleArea'),
                    isActive: self.isActive.bind(self, document.getElementById('freeRuleForm'))
                },
                ruleButton: document.getElementById('ruleButton')
            },
            modal: {
                background: document.getElementById('modalBackground'),
                restart: {
                    container: document.getElementById('restartPopin'),
                    yes: document.getElementById('confirmRestartButton'),
                    no: document.getElementById('cancelRestartButton')
                },
                scores: {
                    container: document.getElementById('scoresPopin'),
                    list: document.getElementById('scoresList')
                }
            }
        };
        return screens;
    }

    bindInteractions() {
        _screens.ruleEditor.numberRuleForm.activator.onclick = this.activateRuleEditor.bind(this, 'dice');
        _screens.ruleEditor.freeRuleForm.activator.onclick = this.activateRuleEditor.bind(this, 'free');
        _screens.navbar.restart.onclick = this.tryRestartGame.bind(this);
        _screens.navbar.scores.onclick = this.displayScores.bind(this);
        _screens.modal.restart.no.onclick = this.hideModal.bind(this);

        _screens.ruleEditor.numberRuleForm.match.onchange = this.checkFormState.bind(this);
        _screens.ruleEditor.numberRuleForm.value.onchange = this.checkFormState.bind(this);
        _screens.ruleEditor.numberRuleForm.sips.onchange = this.checkFormState.bind(this);
        _screens.ruleEditor.freeRuleForm.content.onchange = this.checkFormState.bind(this);
    }

    switchToMainGame() {
        _screens.ruleEditor.container.classList.remove('active');
        _screens.main.container.classList.add('active');
    }

    switchToRuleEditor() {
        _screens.main.container.classList.remove('active');
        _screens.ruleEditor.container.classList.add('active');
    }

    activateRuleEditor(editor) {
        if (editor === 'dice') {
            _screens.ruleEditor.freeRuleForm.container.classList.remove('active');
            _screens.ruleEditor.numberRuleForm.container.classList.add('active');
            _screens.ruleEditor.freeRuleForm.activator.classList.remove('active');
            _screens.ruleEditor.numberRuleForm.activator.classList.add('active');
        }
        else if (editor === 'free') {
            _screens.ruleEditor.numberRuleForm.container.classList.remove('active');
            _screens.ruleEditor.freeRuleForm.container.classList.add('active');
            _screens.ruleEditor.numberRuleForm.activator.classList.remove('active');
            _screens.ruleEditor.freeRuleForm.activator.classList.add('active');
        }
        this.checkFormState();
    }

    setText(target, text) {
        const fragments = target.split('.');
        let element = null;
        while (fragments.length > 0) {
            const fragment = fragments.shift();
            element = (element || _screens)[fragment];
        }
        element.innerHTML = text;
    }

    isActive(element) {
        return element.classList.contains('active');
    }

    updateTurnView(player, dices, button, activeRules, game) {
        _screens.main.game.playerText.innerHTML = `<span class="text-capitalize">${player}</span> - Your roll`;
        _screens.main.game.scoreText.innerHTML = dices.join(' - ');
        _screens.main.game.playButton.innerHTML = button;

        const numberRules = activeRules.filter(r => r.isNumberRule === true)
            .map(r => `<li>${r.displayText(game)}</li>`);
        const freeRules = activeRules.filter(r => r.isFreeRule === true)
            .map(r => `<li>${r.displayText(game)}</li>`);
        _screens.main.rules.dice.innerHTML = numberRules.join(' ');
        _screens.main.rules.free.innerHTML = freeRules.join(' ');
    }

    setPlayButtonCallback(callback) {
        _screens.main.game.playButton.onclick = callback;
    }

    setRuleButtonCallback(callback) {
        _screens.ruleEditor.ruleButton.onclick = callback;
    }

    setRestartButtonCallback(callback) {
        _screens.modal.restart.yes.onclick = () => {
            this.hideModal();
            callback();
        };
    }

    tryRestartGame() {
        this.showModal('restart');
    }

    displayScores() {
        this.showModal('scores');
    }

    showModal(type) {
        _screens.modal.background.classList.add('active');
        _screens.modal.background.onclick = this.hideModal.bind(this);
        _screens.modal[type].container.classList.add('active');
        _screens.modal[type].container.onclick = (e) => {
            e.preventDefault();
            e.stopPropagation();
        };
    } 

    hideModal() {
        _screens.modal.background.classList.remove('active');
        _screens.modal.restart.container.classList.remove('active');
        _screens.modal.scores.container.classList.remove('active');
    }

    resetForms() {
        _screens.ruleEditor.numberRuleForm.condition.value = 'atleast';
        _screens.ruleEditor.numberRuleForm.match.value = 'equal';
        _screens.ruleEditor.numberRuleForm.value.value = '';
        _screens.ruleEditor.numberRuleForm.target.value = 'everyone';
        _screens.ruleEditor.numberRuleForm.sips.value = '1';
        _screens.ruleEditor.freeRuleForm.content.value = '';
        this.checkFormState();
        this.activateRuleEditor('dice');
    }

    setScores(players, scores) {
        let playerData = [];
        for (let i = 0; i < players.length; i++) {
            playerData.push({
                text: `<li class="list-group-item">
                    <div class="row">
                        <div class="col-6">${players[i]}</div>
                        <div class="col-3"><span class="float-right">${scores[i]}</span></div>
                        <div class="col-3"><button onclick="controller.increaseScore(${i}, 1)" type="button" class="btn btn-sm btn-block btn-outline-primary">+1</button></div>
                    </div>
                </li>`,
                score: _scores[i]
            });
        }
        playerData.sort((a, b) => b.score - a.score);
        _screens.modal.scores.list.innerHTML = playerData.map(p => p.text).join(' ');
    }

    checkFormState() {
        let isSubmitActive = true;
        if (_screens.ruleEditor.numberRuleForm.isActive()) {
            const match = _screens.ruleEditor.numberRuleForm.match.value;
            const value = parseInt(_screens.ruleEditor.numberRuleForm.value.value, 10);
            const sips = parseInt(_screens.ruleEditor.numberRuleForm.sips.value, 10);
            if (isNaN(sips) || sips <= 0 
                || (match !== 'even' && match !== 'odd' && (isNaN(value) || value < 1 || value > 6))) {
                isSubmitActive = false;
            }
        }    
        else {
            const content = _screens.ruleEditor.freeRuleForm.content.value;
            if (!content) isSubmitActive = false;
        }   
        _screens.ruleEditor.ruleButton.disabled = !isSubmitActive;
    }

    showRulesList() {
        _screens.main.addRuleFirst.classList.remove('active');
        _screens.main.rules.container.classList.add('active');
    }

    showAddRule() {
        _screens.main.rules.container.classList.remove('active');
        _screens.main.addRuleFirst.classList.add('active');
    }

    updateBiskitIndicator(isBiskit) {
        _screens.main.game.container.classList[isBiskit ? 'add' : 'remove']('biskit');
    }
}