'use strict';

let _game;
let _view;

class GameController {
    constructor(game, view) {
        _game = game;
        _view = view;
        _view.setRestartButtonCallback(this.resetGame.bind(this));
        _view.setScores(_game.players, _game.scores);
    }

    startTurn() {
        _game.initTurn();
        _view.updateTurnView(
            _game.currentPlayer,
            _game.dices,
            'Roll dices',
            _game.activeRules,
            _game
        );
        _view.setPlayButtonCallback(this.playTurn.bind(this));
    }

    playTurn() {
        _game.playTurn();
        _view.updateTurnView(
            _game.currentPlayer,
            _game.dices,
            _game.isPairOfDices ? 'Add rule' : 'Next player',
            _game.activeRules,
            _game
        );
        if (_game.isPairOfDices) _view.showAddRule();
        else _view.showRulesList();
        _view.updateBiskitIndicator(_game.isBiskit);
        _view.setPlayButtonCallback(_game.isPairOfDices ?
            this.startAddRule.bind(this) : this.startNextTurn.bind(this));
    }

    startNextTurn() {
        _game.endTurn();
        _view.setScores(_game.players, _game.scores);
        this.startTurn();
        _view.updateBiskitIndicator(_game.isBiskit);
    }

    startAddRule() {
        _view.setText('ruleEditor.scoreText', _game.dices.join(' - '));
        _view.switchToRuleEditor();
        _view.setRuleButtonCallback(this.performAddRule.bind(this));
    }

    performAddRule() {
        const rule = new Rule(_view.isNumberRule, _view.activeRuleForm);
        _game.submitRule(rule);
        _view.updateTurnView(
            _game.currentPlayer,
            _game.dices,
            'Next player',
            _game.activeRules,
            _game
        );
        _view.showRulesList();
        _view.setPlayButtonCallback(this.startNextTurn.bind(this));
        _view.switchToMainGame();
        _view.resetForms();
    }

    resetGame() {
        _game.setupNewGame(_game.players);
        _view.setScores(_game.players, _game.scores);
        _view.resetForms();
        _view.showRulesList();
        this.startTurn();
    }

    increaseScore(playerIndex, value) {
        _game.increaseScore(_game.players[playerIndex], value);
        _view.setScores(_game.players, _game.scores);
    }
}